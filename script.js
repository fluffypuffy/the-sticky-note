import { nanoid } from "https://cdn.jsdelivr.net/npm/nanoid/nanoid.js";

let stickers = JSON.parse(localStorage.getItem("cachedStickers")) || [];

function renderStickers() {
  const selectedColor = topbarColor.value;
  const selectedImportance = topbarImportance.value;
  const keyWord = topbarInput.value.toLowerCase().trim();

  const filteredStickers = stickers
    .filter((sticker) => sticker.color === selectedColor || !selectedColor)
    .filter(
      (sticker) =>
        sticker.importance === selectedImportance || !selectedImportance
    )
    .filter(
      (sticker) =>
        sticker.description.toLowerCase().includes(keyWord) || !keyWord
    );

  const oldStickers = document.querySelectorAll(".sticky-note");
  oldStickers.forEach((sticker) => sticker.remove());
  filteredStickers.forEach((sticker) => appendStickerToContainer(sticker));
}


const deleteAllButton = document.querySelector(".header__delete-button i");
const topbar = document.querySelector(".header__topbar");

function handleStickyNoteDoubleClick(event) {
  const isItDeleteAllButton = event.composedPath().includes(deleteAllButton);
  const isItTopbar = event.composedPath().includes(topbar);
  const isItSticker = event.target.closest(".sticky-note");
  if (!isItDeleteAllButton && !isItTopbar && !isItSticker) {
    openPopUp(event);
  }
}

const modal = document.querySelector(".modal");

let clickPositionX = 0;
let clickPositionY = 0;

function openPopUp(event) {
  modal.classList.add("open");
  if (!event) return;
  clickPositionX = event.clientX;
  clickPositionY = event.clientY;
}

function closePopUp() {
  modal.classList.remove("open");
}

const modalContent = document.querySelector(".modal__content");

function handleOutsideModalClick(event) {
  const selfClick = event.composedPath().includes(modalContent);
  const isEditButton = event.target.closest(".sticky-note__edit-button");

  if (!selfClick && !isEditButton) {
    closePopUp();
  }
}

const stickerText = document.querySelector(".sticker-text");
const modalButtonOkay = document.querySelector(".modal__button-okay");

function handleStickerTextInput() {
  const isInputEmpty = stickerText.value.trim() === "";
  if (isInputEmpty) {
    modalButtonOkay.disabled = true;
  } else {
    modalButtonOkay.disabled = false;
  }
}

const stickerColorSelect = document.querySelector(".sticker-color");
const stickerIncline = document.querySelector(".sticker-incline");
const stickerImportance = document.querySelector(".sticker-importance");
let modalId = "";

function handleStickerSave() {
  const stickerExist = stickers.find((sticker) => sticker.id === modalId);
  if (!stickerExist) {
    stickers.push({
      id: nanoid(),
      description: stickerText.value,
      color: stickerColorSelect.value,
      incline: stickerIncline.value,
      positionX: clickPositionX,
      positionY: clickPositionY,
      importance: stickerImportance.value,
    });
  } else {
    const existingStickerElement = document.querySelector(
      `[data-id="${modalId}"]`
    );
    existingStickerElement.remove();
    stickers = stickers.map((sticker) => {
      if (sticker.id === modalId) {
        return {
          ...sticker,
          description: stickerText.value,
          color: stickerColorSelect.value,
          incline: stickerIncline.value,
          importance: stickerImportance.value,
        };
      }
      return sticker;
    });
  }

  localStorage.setItem("cachedStickers", JSON.stringify(stickers));
  closePopUp();
  renderStickers();
}

const wrapper = document.querySelector(".wrapper");

function appendStickerToContainer(param) {
  const { id, description, color, incline, positionX, positionY, importance } =
    param;
  const stickerContainer = document.createElement("div");
  stickerContainer.classList.add("sticky-note");
  stickerContainer.setAttribute("data-id", id);

  stickerContainer.style.left = `${positionX}px`;
  stickerContainer.style.top = `${positionY}px`;
  stickerContainer.style.transform = `rotate(${incline}deg)`;

  stickerContainer.innerHTML = `
      <div class="sticky-note__content">
        <div class="sticky-note__content-outer">
          <div class="sticky-note__content-inner">
            <svg width="0" height="0">
              <defs>
                <clipPath id="stickyClip" clipPathUnits="objectBoundingBox">
                  <path
                    d="M 0 0 Q 0 0.69, 0.03 0.96 0.03 0.96, 1 0.96 Q 0.96 0.69, 0.96 0 0.96 0, 0 0"
                    stroke-linejoin="round"
                    stroke-linecap="square"
                  />
                </clipPath>
              </defs>
            </svg>
            <div class="sticky-note__main-content color_${color}">
              <div class="sticky-note__buttons">
                <div class="sticky-note__delete-button"><i class="fa-solid fa-trash-can" style="color: #2c3632;"></i></div>
                <div class="sticky-note__edit-button"><i class="fa-regular fa-pen-to-square" style="color: #2c3632;"></i></div>
              </div>
              <div class="sticky-note__text text-importance_${importance}">
              ${description}
              </div>
            </div>
          </div>
        </div>
      </div>
    `;

  wrapper.appendChild(stickerContainer);

  const stickyButtonDelete = stickerContainer.querySelector(
    ".sticky-note__delete-button"
  );

  function handleStickyDeleteClick(event) {
    const clickedSticker = event.target;
    const clickedContainerInner = clickedSticker.closest(".sticky-note");

    if (clickedContainerInner) {
      const stickerId = clickedContainerInner.getAttribute("data-id");
      const indexOfSticker = stickers.findIndex(function (sticker) {
        return sticker.id === stickerId;
      });
      stickers.splice(indexOfSticker, 1);
      localStorage.setItem("cachedStickers", JSON.stringify(stickers));
      clickedContainerInner.remove();
    }
  }

  stickyButtonDelete.addEventListener("click", handleStickyDeleteClick);

  const stickyButtonEdit = stickerContainer.querySelector(
    ".sticky-note__edit-button"
  );

  function handleStickyEditClick(event) {
    const clickedSticker = event.target;
    const clickedContainerInner = clickedSticker.closest(".sticky-note");
    const stickerId = clickedContainerInner.getAttribute("data-id");
    const clickedStickerData = stickers.find(function (sticker) {
      return sticker.id === stickerId;
    });
    fillPopUpWithData(clickedStickerData);
    openPopUp();
    modalId = stickerId;
    modalButtonOkay.disabled = false;
  }

  stickyButtonEdit.addEventListener("click", handleStickyEditClick);
}

const topbarInput = document.querySelector(".topbar__input");
const topbarColor = document.querySelector("#select-color");
const topbarImportance = document.querySelector("#select-importance");

function handleTopbarButtonClickSearch(event) {
  event.preventDefault();
  renderStickers();
}

function handleTopbarButtonClickClear(event) {
  event.preventDefault();
  topbarColor.value = "";
  topbarImportance.value = "";
  topbarInput.value = "";
  renderStickers();
}

function fillPopUpWithData(param) {
  stickerText.value = param.description;
  stickerColorSelect.value = param.color;
  stickerIncline.value = param.incline;
  stickerImportance.value = param.importance;
}

function handleDeleteAllClick() {
  localStorage.removeItem("cachedStickers");
  const stickerContainers = document.querySelectorAll(".sticky-note");
  stickerContainers.forEach((container) => {
    container.remove();
  });
}

renderStickers();

document.addEventListener("dblclick", handleStickyNoteDoubleClick);
document.addEventListener("click", handleOutsideModalClick);

const modalCloseIcon = document.querySelector("#close-button");
modalCloseIcon.addEventListener("click", closePopUp);

const modalButtonCancel = document.querySelector(".modal__button-cancel");
modalButtonCancel.addEventListener("click", closePopUp);

modalButtonOkay.addEventListener("click", handleStickerSave);

stickerText.addEventListener("input", handleStickerTextInput);

deleteAllButton.addEventListener("click", handleDeleteAllClick);

const topbarButtonSearch = document.querySelector(".topbar__button-search");
const topbarButtonClear = document.querySelector(".topbar__button-clear");

topbarButtonSearch.addEventListener("click", handleTopbarButtonClickSearch);
topbarInput.addEventListener("blur", renderStickers);
topbarColor.addEventListener("blur", renderStickers);
topbarImportance.addEventListener("blur", renderStickers);
topbarButtonClear.addEventListener("click", handleTopbarButtonClickClear);
