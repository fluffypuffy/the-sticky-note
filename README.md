# The Sticky Note

Welcome to "The Sticky Note", a dynamic and interactive web application designed to improve your productivity and organization skills. Using digital sticky notes, you can effortlessly jot down tasks, ideas, and reminders, making it easier than ever to keep track of your daily obligations.

## Features

- **Interactive Sticky Notes**: Create digital sticky notes that you can place anywhere on your screen.
- **Customization Options**: Choose different colors and set the importance for each note to categorize and prioritize your tasks effectively.
- **Search and Filter**: Utilize the search bar and filters to easily find notes by their color, importance, or content.
- **Persistent Storage**: Thanks to localStorage integration, your notes will stay right where you left them, even after closing the browser.
- **Fully Responsive**: Designed to work flawlessly on both desktop and mobile devices.

## How to Use

- **Creating a Note**: Simply double-click anywhere on the screen to open the note creation modal. Fill in your note details and click 'Okay' to add it to the screen.
- **Managing Notes**: You can search for specific notes using keywords, or filter them by color and importance using the controls in the top bar.
- **Editing and Deleting**: Double-click on an existing note to edit its details or click the delete icon to remove it.

## Built With

- **HTML5 & CSS3** for structure and styling.
- **Vanilla JavaScript** for functionality.
- **nanoid** for generating unique IDs for each sticky note.

## Screenshot

![The Sticky Note Screenshot](01.png)

## Contact

- **Stanislava** - stasmagergut@gmail.com

## Links

- https://the-sticky-note-fluffypuffy-12b0efbf6784904ee9db3121e1793dbd303.gitlab.io/
- http://surl.li/rxdvu
